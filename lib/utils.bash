#!/usr/bin/env bash

set -euo pipefail

#GH_REPO="https://github.com/dat-linux/releases-weka"
TOOL_NAME="weka"
TOOL_TEST="weka"

fail() {
  echo -e "cari-$TOOL_NAME: $*"
  exit 1
}

curl_opts=(-fSL#)

if [ -n "${GITHUB_API_TOKEN:-}" ]; then
  curl_opts=("${curl_opts[@]}" -H "Authorization: token $GITHUB_API_TOKEN")
fi

sort_versions() {
  sed 'h; s/[+-]/./g; s/.p\([[:digit:]]\)/.z\1/; s/$/.z/; G; s/\n/ /' |
    sort -t. -k 1,1n -k 2,2n -k 3,3n -k 4,4n | awk '{print $2}'
}

#list_github_tags() {
#  git ls-remote --tags --refs "$GH_REPO" |
#    grep -o 'refs/tags/.*' | cut -d/ -f3- |
#    sed 's/^v//' | grep -v "3.9.6"
#}

# Add new vers as required..
list_all_versions() {  
	#list_github_tags
  echo "3.8.6
3.9.6"
}

download_release() {
  local version filename url
  version="$1"
  filename="$2"
  rm -rf "$filename" &
  arrIN=(${version//\./ })
  ver="${arrIN[0]}.${arrIN[1]}"
  maj_ver="`echo "${version}" | cut -d"." -f1`"
  min_ver="`echo "${version}" | cut -d"." -f2`"
  min_min_ver="`echo "${version}" | cut -d"." -f3`"
  #https://netix.dl.sourceforge.net/project/weka/weka-3-9/3.9.6/weka-3-9-6-azul-zulu-linux.zip
  url="https://netix.dl.sourceforge.net/project/weka/weka-${maj_ver}-${min_ver}/${version}/weka-${maj_ver}-${min_ver}-${min_min_ver}-azul-zulu-linux.zip"
  #https://ixpeering.dl.sourceforge.net/project/weka/weka-3-9/3.9.6/weka-3-9-6-azul-zulu-linux.zip
  url2="https://ixpeering.dl.sourceforge.net/project/weka/weka-${maj_ver}-${min_ver}/${version}/weka-${maj_ver}-${min_ver}-${min_min_ver}-azul-zulu-linux.zip"
  #url="$GH_REPO/releases/download/v${version}/weka-${maj_ver}-${min_ver}-${min_min_ver}-azul-zulu-linux.zip"
  #min_ver="`echo "${version}" | cut -d"." -f2`"
  #url="https://netcologne.dl.sourceforge.net/project/weka/weka-3-${min_ver}/${version}/weka-${version}-azul-zulu-linux.zip"
  echo "* Downloading $TOOL_NAME release $version..."
  echo "\"${curl_opts[@]}\" -o \"$filename\" -C - \"$url\""
  curl "${curl_opts[@]}" -o "$filename" -C - "$url" || curl "${curl_opts[@]}" -o "$filename" -C - "$url2" || fail "Could not download $url"
  printf "\\n"
}

install_version() {
  local install_type="$1"
  local version="$2"
  local install_path="$3"

  if [ "$install_type" != "version" ]; then
    fail "cari-$TOOL_NAME supports release installs only!"
  fi

  maj_ver="`echo "${version}" | cut -d"." -f1`"
  min_ver="`echo "${version}" | cut -d"." -f2`"
  min_min_ver="`echo "${version}" | cut -d"." -f3`"
  
  (
    mkdir -p "$install_path/bin"
    cp -r "$CARI_DOWNLOAD_PATH"/* "$install_path"
    touch "$install_path/bin/weka"
    echo "#!/bin/bash" >> "$install_path/bin/weka"
    echo "cd $install_path/weka-${maj_ver}-${min_ver}-${min_min_ver}/ && sh weka.sh" >> "$install_path/bin/weka"
    chmod a+x "$install_path/bin/weka"
    test -x "$install_path/bin/$TOOL_TEST" || fail "Expected $install_path/bin/$TOOL_TEST to be executable."
    echo "$TOOL_NAME $version installation was successful!"
  ) || (
    rm -rf "$install_path"
    fail "An error ocurred while installing $TOOL_NAME $version."
  )
}

post_install() {
  rm -rf "$CARI_DOWNLOAD_PATH/"
}
